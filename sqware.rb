require "swar" # https://gitlab.com/n8chz/swar
require "array-symmetries"
require "gensym"

class Sqware
  attr_reader :weights
  def initialize(swar)
    @swar = swar
    @vertices = @swar.graph.keys-[@swar.far_point]
    @dist = create_dist_table
    @weights = create_weight_table
    @centralities = create_centralities_table
    random_placement!
  end

  def display
    width = @vertices.map(&:length).max
    (0...@grid_size).map do |row|
      (0...@grid_size).map do |col|
        # sym = @placement.key([row, col])
        # @centralities[sym].to_s.rjust(width)
        (@placement.key([row, col]).to_s || "").ljust(width)
      end.join "  "
    end.join "\n"
  end

  def random_placement!
    squares = (0...@grid_size).to_a.product((0...@grid_size).to_a)
    @placement = @vertices.zip(squares.shuffle).to_h
    @empties = squares-@placement.values
    self
  end

  def quality
    @vertices.sort.combination(2).map do |v1, v2|
      [distance(v1, v2), -@weights[[v1, v2]]]
    end.corr
  end

  def monte_carlo!(timeout = 60.0)
    # puts "Number of distinct placements: #{n_placements}"
    # squares = (0...@grid_size).to_a.product((0...@grid_size).to_a)
    start_time = Time.new.to_f
    n = 0
    q = quality
    loop do
      p = @placement.clone
      random_placement!
      q2 = quality
      if q2 > q
        q = q2
      else
        @placement = p
      end
      n += 1
      break if Time.new.to_f-start_time > timeout
    end
    t = "%.3f"%(Time.new.to_f-start_time)
    puts "Number of placements tried in #{t} seconds: #{n}"
    puts "Achieved quality: #{q}"
    # self
  end

  def reassignment! # This is not very fruitful
    lcv = least_comfortable_vertex
    p = @placement.clone
    q1 = quality
    cell, q2 = @empties.map do |empty_cell|
      @placement[lcv] = empty_cell
      [empty_cell, quality]
    end.max_by {|_, q| q}
    if q2 > q1
      @empties.push(@placement[lcv])
      @placement[lcv] = cell
      puts "managed to eke out an improvement to quality level #{q2}"
    else
      @placement = p
    end
    # self
  end

  def brute_force! # not at all practical, of course
    squares = (0...@grid_size).to_a.product((0...@grid_size).to_a)
    n_layouts = perm(squares.count, @vertices.count)
    n_tested = 0
    start_time = Time.new.to_f
    q = quality
    squares.permutation(@vertices.count).each do |prm|
      p = @placement.clone
      @placement = @vertices.zip(prm).to_h
      # filter out redundant trial placements:
      # Doesn't work yet.
=begin
      placement_matrix = (0...@grid_size).map do |row|
        (0...@grid_size).map do |col|
          @placement.key([row, col])
        end
      end
      firsts = placement_matrix.symmetries.map(&:first).map(&:first).map do |e|
        e.class == Symbol ? e : :was_nil
      end
      next unless firsts.min == firsts.first
=end
      q2 = quality
      if q2 > q
        q = q2
        puts "achieved quality level of #{q}"
      else
        @placement = p
      end
      n_tested += 1
      puts "#{n_tested} of #{n_layouts} after #{"%.3f"%(Time.new.to_f-start_time)} seconds" if (n_tested%1000).zero?
    end
  end

  def improve!
    # p = @placement.clone
    q = quality
    loop do
      random_placement!
      break if quality > q
    end
  end

  private
  def n_placements
    choose(@grid_size*@grid_size, @vertices.count)/8
  end

  def choose(n, r)
    k = [r, n-r].min
    (1...k).reduce(n) do |c, i|
      c*(n-i)/(i+1)
    end
  end

  def perm(n, r)
    (1...r).reduce(n) do |c, i|
      c*(n-i)
    end
  end

  def distance(v1, v2)
    x1, y1 = @placement[v1]
    x2, y2 = @placement[v2]
    dx = (x1-x2).abs
    dy = (y1-y2).abs
    @dist[dx][dy]
  end

  def tension(vertex, square)
    (@vertices-[vertex]).reduce(0) do |t, v|
      t+@swar.weight(vertex, v)*distance(vertex, v)
    end
  end

  def create_dist_table
    @vertex_count = @vertices.count
    @grid_size = Math.sqrt(@vertex_count+1).ceil
    # invest some cpu cycles now to not spend later on square roots:
    (0...@grid_size).map do |x|
      (0...@grid_size).map do |y|
        Math.sqrt(x*x+y*y)
      end
    end
  end

  def create_weight_table
    @vertices.sort.combination(2).map do |v1, v2|
      [[v1, v2], @swar.weight(v1, v2)]
    end.to_h
  end

  def create_centralities_table
    @weights.each_with_object({}) do |weight_entry, acc|
      vertex_pair, weight = weight_entry
      vertex_pair.each do |v|
        acc[v] ||= 0
        acc[v] += weight
      end
    end
  end

  def least_comfortable_vertex
    @vertices.min_by do |v1|
      (@vertices-[v1]).map do |v2|
        f, s = [v1, v2].sort
        [distance(f, s), -(@weights[[f, s]] || 0)]
      end.corr
    end
  end
end

class Array
  def corr
    n, sx, sy, sxy, sx2, sy2 = [0, 0, 0, 0, 0, 0]
    each do |x, y|
      n += 1
      sx += x
      sy += y
      sxy += x*y
      sx2 += x*x
      sy2 += y*y
    end
    (n*sxy-sx*sy)/Math.sqrt((n*sx2-sx*sx)*(n*sy2-sy*sy))
  end
end
